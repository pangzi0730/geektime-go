module gitee.com/pangzi0730/geektime-go

go 1.19

require (
	gitee.com/geektime-geekbang/geektime-go v0.0.0-20220817145548-f4e81a9b3fef
	github.com/stretchr/testify v1.8.0
	golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
